<?php

namespace ComposePress\Views\Engine;

use ComposePress\Core\Abstracts\Component_0_9_0_0;
use ComposePress\Views\Interfaces\ViewEngine;

class WordPress_0_2_0_0 extends Component_0_9_0_0 implements ViewEngine {
	public function is_overridable( $file ) {
		$data = get_file_data( $file, [ 'Overrideable' => 'Overrideable' ] );
		if ( in_array( strtolower( $data['Overrideable'] ), [ 'yes', 'true' ] ) ) {
			$data['Overrideable'] = true;
		}
		if ( in_array( strtolower( $data['Overrideable'] ), [ 'no', 'false' ] ) ) {
			$data['Overrideable'] = false;
		}

		return $data['Overrideable'];
	}

	public function get_file_extension() {
		return 'php';
	}

	public function is_buffered() {
		return true;
	}

	public function render( $view, $data = array(), $return = false ) {
		$this->wp_query->query_vars = array_merge( $this->wp_query->query_vars, $data );
		load_template( $view, false );
	}
}
