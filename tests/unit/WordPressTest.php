<?php

/**
 * Class ViewTest
 */
class WordPressTest extends \Codeception\TestCase\WPTestCase {

	/**
	 * @var \ComposePress\Views\View_0_5_0_0
	 */
	private $view;
	/**
	 * @var \Test_Plugin
	 */
	private $plugin;

	/**
	 * @throws \ComposePress\Core\Exception\Plugin
	 */
	public function setUp() {
		// before
		parent::setUp();
		$this->plugin = test_plugin();
		$this->view   = $this->plugin->create_component( '\ComposePress\Views\View_0_5_0_0' );
	}

	/**
	 *
	 */
	public function tearDown() {
		$this->view = null;
		parent::tearDown();
	}

	/**
	 *
	 * @throws \Exception
	 */
	public function test_basic() {
		$this->view->init();
		ob_start();
		$this->view->render( 'hello' );
		$this->assertEquals( 'world', trim( ob_get_clean() ) );
	}


	/**
	 *
	 * @throws \Exception
	 */
	public function test_data() {
		$this->view->init();
		ob_start();
		$this->view->render( 'hello_data', [ 'name' => 'john' ] );
		$this->assertEquals( 'hello john', trim( ob_get_clean() ) );
	}

	/**
	 *
	 * @throws \Exception
	 */
	public function test_override_missing() {
		$this->view->init();
		ob_start();
		$this->view->render( 'hello_overrideable' );
		$this->assertEquals( 'world', trim( ob_get_clean() ) );
	}

	/**
	 *
	 * @throws \Exception
	 */
	public function test_missing_template() {
		$this->expectException( '\ComposePress\Views\Exceptions\ViewException' );
		$this->view->init();
		$this->view->render( '404' );
	}

	/**
	 *
	 * @throws \Exception
	 */
	public function test_override() {
		$this->view->init();
		ob_start();
		$this->view->render( 'hello_overrideable' );
		$this->assertEquals( 'world', trim( ob_get_clean() ) );
	}

	/**
	 *
	 * @throws \Exception
	 */
	public function test_set_view_path() {
		$this->view->init();
		$this->view->view_path = 'test';
		$this->assertEquals( 'test/', $this->view->view_path );
	}

	public function test_bad_data() {
		$this->expectException( '\ComposePress\Views\Exceptions\ViewException' );
		$this->view->init();
		$this->view->render( 'test', false );
	}

	public function test_basic_return() {
		$this->view->init();
		$this->assertEquals( 'world', trim( $this->view->render( 'hello', [], true ) ) );
	}

	/**
	 *
	 * @throws \Exception
	 */
	public function test_overridden() {
		$this->view->init();
		$dir = STYLESHEETPATH . '/' . $this->view->overridable_path;

		if ( ! $this->plugin->wp_filesystem->is_dir( $dir ) ) {
			$this->assertTrue( $this->plugin->wp_filesystem->mkdir( $dir ) );
		}

		$this->plugin->wp_filesystem->copy( $this->view->locate( 'hello_overridden' ), STYLESHEETPATH . '/' . $this->view->get_overridable_view( 'hello_overrideable' ), true );
		ob_start();

		$this->view->render( 'hello_overrideable' );

		$this->assertEquals( 'hello', trim( ob_get_clean() ) );
		$this->assertTrue( $this->plugin->wp_filesystem->delete( STYLESHEETPATH . '/' . $this->view->get_overridable_view( 'hello_overrideable' ) ) );
	}
}
