<?php

/* @var $container \ComposePress\Dice\Dice */

$container = $container->addRule( '\Test_Plugin', [
	'shared' => true,
] );
$container = $container->addRule( 'ComposePress\Views\View_0_5_0_0',
	[
		'substitutions' => [
			'ComposePress\Views\Interfaces\ViewEngine' => '\ComposePress\Views\Engine\WordPress_0_2_0_0',
		],
	] );
